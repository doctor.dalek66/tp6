package fr.umfds.tp6;

import java.util.HashMap;
import java.util.*;

public class Groupe {
	private int id;
	private String nom;
	private List<Etudiant> membreGroupe = new ArrayList<>();
	private HashMap<Integer,Voeux> choixVoeux = new HashMap<>(); 
	private int i = 1;
	private Sujet affectation;

	public Groupe(int id, String nom) {
		this.id = id;
		this.nom = nom;
	}
	
	public void addToGroupe(Etudiant e) {
		if((membreGroupe.size()<5) && (!(membreGroupe.contains(e)))) {
			membreGroupe.add(e);
			e.setAffectation(this);
		}
	}
	
	public void faireVoeux(Voeux v) {
		if((i < 5) && (!(choixVoeux.containsValue(v)))) {
			choixVoeux.put(i, v);
			i++;
		}
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public List<Etudiant> getMembreGroupe() {
		return membreGroupe;
	}

	public Map<Integer, Voeux> getChoixVoeux() {
		return choixVoeux;
	}

	public Sujet getAffectation() {
		return affectation;
	}

	public void setAffectation(Sujet affectation) {
		this.affectation = affectation;
	}


	
	
}
