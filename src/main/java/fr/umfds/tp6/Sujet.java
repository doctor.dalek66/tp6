package fr.umfds.tp6;

public class Sujet {
	private String titre;
	private String resume;
	private Enseignant encadrant;
	private Groupe groupeAffecte;
	boolean affecte  = false;

	public Sujet(String titre, String resume, Enseignant encadrant) {
		this.titre = titre;
		this.resume = resume;
		this.encadrant = encadrant;
	}
	
	public String getTitre() {
		return titre;
	}

	public void setTitre(String titre) {
		this.titre = titre;
	}

	public String getResume() {
		return resume;
	}

	public void setResume(String resume) {
		this.resume = resume;
	}

	public Enseignant getEncadrant() {
		return encadrant;
	}

	public void setEncadrant(Enseignant encadrant) {
		this.encadrant = encadrant;
	}
	
	public Groupe getGroupeAffecte() {
		return groupeAffecte;
	}

	public void setGroupeAffecte(Groupe groupeAffecte) {
		this.groupeAffecte = groupeAffecte;
		this.affecte = true;
	}
	
	public boolean isAffecte() {
		return this.groupeAffecte != null;
	}

}
