package fr.umfds.tp6;

public class Voeux {
	private Sujet sujet;

	public Voeux(Sujet sujet) {
		super();
		this.sujet = sujet;
	}

	public Sujet getSujet() {
		return sujet;
	}

	public void setSujet(Sujet sujet) {
		this.sujet = sujet;
	}
	

}
