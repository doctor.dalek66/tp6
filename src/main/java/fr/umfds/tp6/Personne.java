package fr.umfds.tp6;
public class Personne {
	private String nom;
	private String prenom;
	private String mail;
	
	public Personne(String n, String p, String m) {
		setNom(n);
		setPrenom(p);
		setMail(m);
	}

	public String getMail() {
		return mail;
	}
	
	public String getPrenom() {
		return prenom;
	}
	
	public String getNom() {
		return nom;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}


	public void setNom(String nom) {
		this.nom = nom;
	}
}
