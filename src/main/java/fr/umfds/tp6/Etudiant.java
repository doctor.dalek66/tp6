package fr.umfds.tp6;

public class Etudiant extends Personne {
	private int numEt;
	private Groupe affectation;

	public Etudiant(String n, String p, String m, int e) {
		super(n, p, m);
		numEt = e;
	}

	public int getNumEt() {
		return numEt;
	}
	
	public Groupe getAffectation() {
		return affectation;
	}

	public void setNumEt(int numEt) {
		this.numEt = numEt;
	}
	
	public void setAffectation(Groupe affectation) {
		this.affectation = affectation;
	}
	
	
}
