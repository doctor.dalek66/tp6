package fr.umfds.tp6;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;

class AppTest {
	
	Etudiant e1 = new Etudiant("e","1","e.1@etu.umontpellier.fr",218);
	Etudiant e2 = new Etudiant("e","2","e.2@etu.umontpellier.fr",228);
	Etudiant e3 = new Etudiant("e","3","e.3@etu.umontpellier.fr",238);
	Etudiant e4 = new Etudiant("e","4","e.4@etu.umontpellier.fr",248);
	Etudiant e5 = new Etudiant("e","5","e.5@etu.umontpellier.fr",258);
	Enseignant ee1 = new Enseignant("e","e1","ee.1@etu.umontpellier.fr");
	Sujet s1 = new Sujet("...", null, ee1); Voeux v1 = new Voeux(s1);
	Sujet s2 = new Sujet("...", null, ee1); Voeux v2 = new Voeux(s2);
	Sujet s3 = new Sujet("...", null, ee1); Voeux v3 = new Voeux(s3);
	Sujet s4 = new Sujet("....", null, ee1); Voeux v4 = new Voeux(s4);
	Sujet s5 = new Sujet("...", null, ee1); Voeux v5 = new Voeux(s5);
	Sujet s6 = new Sujet("...", null, ee1); Voeux v6 = new Voeux(s6);
	Sujet s7 = new Sujet("...", null, ee1); Voeux v7 = new Voeux(s7);
	Groupe g1 = new Groupe(1,"1d6");
	Groupe g2 = new Groupe(2,"2d7");
	
	@BeforeEach
	void init()  {
		  g1.addToGroupe(e1);g1.addToGroupe(e2);g1.addToGroupe(e3);
		  g2.addToGroupe(e4);g2.addToGroupe(e5);
		  g1.faireVoeux(v1); g1.faireVoeux(v2); g1.faireVoeux(v3); g1.faireVoeux(v4);
		  g2.faireVoeux(v7); g2.faireVoeux(v6); g2.faireVoeux(v5); g2.faireVoeux(v4); g2.faireVoeux(v3);
		  g1.setAffectation(s1);
		  s1.setGroupeAffecte(g1);
	}
	
	@Test
	void G1dansS1() {
		assertEquals(s1.getGroupeAffecte(),g1);
	}
	
	@Test
	void S1dansG1() {
		assertEquals(g1.getAffectation(),s1);
	}
	
	@Test
	void S1estAffecte() {
		assertTrue(s1.isAffecte());
	}


}
